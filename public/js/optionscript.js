$(document).ready(function () {
    $("#form1").submit(function () {
        if ($("#sortByName").val() == "asc") {
            $("#sortByName").remove();
        }
        if ($("#perPage").val() == "20") {
            $("#perPage").remove();
        }
    });
});

$(function () {
    $('#sortByName').change(function () {
        localStorage.clear()
        localStorage.setItem('todoData', this.value);
    });
    $('#perPage').change(function () {
        localStorage.clear()
        localStorage.setItem('perData', this.value);
    });

    $('#byYear').change(function () {
        localStorage.clear()
        localStorage.setItem('byYear', this.value);
    });

    $('#byPrice').change(function () {
        localStorage.clear()
        localStorage.setItem('byPrice', this.value);
    });
    if (localStorage.getItem('perData')) {
        $('#perPage').val(localStorage.getItem('perData'));

    }
    if (localStorage.getItem('todoData')) {
        $('#sortByName').val(localStorage.getItem('todoData'));
    }
    if (localStorage.getItem('byYear')) {
        $('#byYear').val(localStorage.getItem('byYear'));
    }

    if (localStorage.getItem('byPrice')) {
        $('#byPrice').val(localStorage.getItem('byPrice'));

    }
});


function changeByYear() {
    localStorage.clear()
    document.getElementById("form").submit();
}

function changeByPrice() {
    localStorage.clear()
    document.getElementById("form2").submit();
}