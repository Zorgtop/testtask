<?php


class ProductsTableSeeder extends Seeder
{

    public function run()
    {


        $this->command->info('Deleting existing Products table ...');
        DB::table('products')->delete();

        $faker = Faker\Factory::create('ru_RU');

        for ($i = 0; $i < 500; $i++) {

            $threads = [
                [
                    'id' => $faker->randomNumber($nbDigits = NULL, $strict = false),
                    'name' => $faker->word,
                    'price' => $faker->numberBetween(10, 10000),
                    'description' => $faker->sentence,
                    'year' => $faker->year,
                    'created_at'=>$faker->dateTimeBetween('-4 month', '-1 month '),
                    'updated_at'=>$faker->dateTimeBetween('-4 month', 'now')
                ]

            ];
            DB::table('products')->insert($threads);

        }

    }

}