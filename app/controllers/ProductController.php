<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;

use \App\Models\Product;

use Illuminate\Http\Request;


class ProductController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function index()
    {

        $perpage = Input::get('perPage');
        $sort = Input::get('sortByName');
        $byPrice = Input::get('byPrice');
        $byYear = Input::get('byYear');
        if ($sort == null) {
            $sort = "asc";
        }

        if ($perpage == null) {
            $perpage = 20;
        }
        if ($byPrice == null && $byYear == null) {

            $products = Product::orderBy('name', $sort)
                ->paginate($perpage)
                ->appends(Input::except('page'));
        } else if ($byPrice != null) {
            $products = Product::orderBy('price', $byPrice)
                ->paginate($perpage)
                ->appends(Input::except('page'));

        } else if ($byYear != null) {
            $products = Product::orderBy('year', $byYear)
                ->paginate($perpage)
                ->appends(Input::except('page'));

        }


        return \View::make('hello')->withProducts($products);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProduct(Request $request)

    {

        /*
        $categories = Product::paginate(5);

        if ($request->ajax()) {

            return \View::make('product', compact('categories'));

        }

        return \View::make('product_list', compact('categories'));*/
        $products = Product::paginate(20);
        return \View::make('hello')->withProducts($products);

    }


}
