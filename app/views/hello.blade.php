<html>
<header>
    {{ HTML::style('css/style.css') }}
    <title>Datatables Server Side Processing in Laravel</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"/>
    <script src="{{ asset('js/optionscript.js') }}"></script>
</header>
<body>
<h1>Products</h1>
<div class="container">
    <div class="panel panel-heading">All Products({{ $products->count() }})
        <form action="/products" method="get" id="form1">
            <p><select class="form-control" id="sortByName" name="sortByName">
                    <option disabled selected value>Sort by alphabet</option>
                    <option selected value="asc">По возрастанию</option>
                    <option value="desc">По убыванию</option>
                </select></p>


            <p><select class="form-control" id="perPage" name="perPage">
                    <option disabled selected value>Change elements list</option>
                    <option>50</option>
                    <option value="10">10</option>
                    <option selected value="20">20</option>
                </select></p>
            <p><input type="submit" value="Количество элементов"></p>
        </form>
    </div>

    <form action="/products" method="get" id="form">
        <select class="form-control" id="byYear" name="byYear" onchange="changeByYear()">
            <option disabled selected value>По годам</option>
            <option value="asc">от старых</option>
            <option value="desc">от новых</option>
        </select>
    </form>
    <form action="/products" method="get" id="form2">
        <select class="form-control" id="byPrice" name="byPrice" onchange="changeByPrice()">
            <option disabled selected value>По цене</option>
            <option value="asc">по возрастанию (цены)</option>
            <option value="desc">по убыванию (цены)</option>
        </select>
    </form>
</div>
<div class="container">
    @if(isset($products))
        <h2>Sample Products details</h2>
        {{ $products->links() }}
        <table id="product_table" class="table table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Year</th>
                <th>Created at</th>
                <th>Updated at</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $dummy)
                <tr>
                    <td>{{$dummy->name}}</td>
                    <td>{{$dummy->price}}</td>
                    <td>{{$dummy->description}}</td>
                    <td>{{$dummy->year}}</td>
                    <td>{{$dummy->created_at}}</td>
                    <td>{{$dummy->updated_at}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
        {{ $products->links() }}
    @endif
</div>

</body>
</html>
